package com.csci.zillowservice.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class,property = "@id",scope = Query.class)
@Data
@ToString
@NoArgsConstructor
public class Query {
    private String title;
    private String description;
    public Query(String title, String description){
        this.title = title;
        this.description = description;
    }
}
