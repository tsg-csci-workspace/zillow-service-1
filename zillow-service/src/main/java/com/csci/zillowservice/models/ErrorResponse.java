package com.csci.zillowservice.models;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import java.time.LocalDateTime;

@Getter
@Setter
@Schema(description = "Error Response")
public class ErrorResponse {
    private boolean success;
    private HttpStatus status;
    private LocalDateTime timestamp;
    private String message;
    private String details;

    private ErrorResponse(){
        timestamp = LocalDateTime.now();
        success = false;
        status = HttpStatus.BAD_REQUEST;
    }

    public ErrorResponse(HttpStatus status, String message, Throwable ex){
        this();
        this.status = status;
        this.success = false;
        this.message = message;
        this.details = ex.getLocalizedMessage();
    }

}
