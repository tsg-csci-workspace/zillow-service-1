package com.csci.zillowservice.models;

public class Properties {

    private boolean success;
    private Number status;
    private Property[] bundle;
    private Number total;

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public void setStatus(Number status) {
        this.status = status;
    }

    public void setBundle(Property[] bundle) {
        this.bundle = bundle;
    }

    public void setTotal(Number total) {
        this.total = total;
    }

    public boolean isSuccess() {
        return success;
    }

    public Number getStatus() {
        return status;
    }

    public Property[] getBundle() {
        return bundle;
    }

    public Number getTotal() {
        return total;
    }


}
