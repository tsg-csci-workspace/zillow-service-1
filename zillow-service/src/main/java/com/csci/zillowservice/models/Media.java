package com.csci.zillowservice.models;

public class Media {
        public String ClassName;
        public String MediaCategory;
        public String MediaKey;
        public String MediaObjectID;
        public String MediaURL;
        public String MimeType;
        public Number Order;
        public String ResourceName;
        public String ResourceRecordKey;
        public String ShortDescription;
}
