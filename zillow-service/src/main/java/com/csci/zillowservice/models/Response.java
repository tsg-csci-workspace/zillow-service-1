package com.csci.zillowservice.models;

public class Response {
    public String ListingId;
    public Number ListPrice;
    public Number Latitude;
    public Number Longitude;
    public Number RoomsTotal;
    public Number BedroomsTotal;
    public Number BathroomsTotalDecimal;
    public Number YearBuilt;
    public Number DaysOnMarket;
    public Number LivingArea;
}

