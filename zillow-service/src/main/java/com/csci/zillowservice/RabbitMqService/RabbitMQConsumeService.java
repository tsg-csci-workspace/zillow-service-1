package com.csci.zillowservice.RabbitMqService;

import com.csci.zillowservice.models.Query;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQConsumeService {
    private static final String QUEUE="item_queue";

    @RabbitListener(queues = QUEUE)
    public void recieveMessage(Query query){
        System.out.println("Recieved message from rabbitmq > " + query);
    }
}
