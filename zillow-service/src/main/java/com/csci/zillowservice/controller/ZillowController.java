package com.csci.zillowservice.controller;

import com.csci.zillowservice.handlers.RestExceptionHandler;
import com.csci.zillowservice.models.ErrorResponse;
import com.csci.zillowservice.models.Payload;
import com.csci.zillowservice.models.Properties;
import com.csci.zillowservice.models.Properties;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.validation.ConstraintViolationException;


@Validated
@RestController
@CrossOrigin
@RequestMapping("/v1/test/")
public class ZillowController {

    @Value("${api.key}")
    private String apiKey;
    @Value("${base.url}")
    private String baseURL;

    @Autowired
    private WebClient.Builder webClientBuilder;

    @ApiOperation(value = "Get sample of Zillow data.", response = Iterable.class)
    @GetMapping("zillowTest")
    public ResponseEntity<Properties> getZillowData(@RequestParam @Range(min = 1, max = 200) Integer limit){

        String url = baseURL + apiKey + "&limit=" + limit;
        Properties properties = webClientBuilder.build()
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(Properties.class)
                .block();
        return new ResponseEntity<>(properties, HttpStatus.ACCEPTED);
    }
        /**
     *
     * @param query
     * @return payload
     */
    public static String ProcessQuery(String query){
        String payload = query.replace(",","&");
        return "&" + payload;
    }
    /**
     *
     * @param sortBy
     * @param longitude
     * @param latitude
     * @param order
     * @param near
     * @param offset
     * @param limit describes how many entries should be returned from bridge api
     * @param radius describes the size of the circle drawn on the map
     * @param fields used to describe what data attributes should be returned from bridge api
     * @param query
     * @return
     */
    @ApiOperation(value = "Get Zillow data with full query", response = Iterable.class)
    @GetMapping("zillow/search")
    public ResponseEntity<Payload> getData(@RequestParam(required = false) String sortBy,
                                           @RequestParam(required = false) @Range(min = -180, max = 180) Number longitude,
                                           @RequestParam(required = false) @Range(min = -90, max = 90) Number latitude,
                                           @RequestParam(required = false) String order,
                                           @RequestParam(required = false) String near,
                                           @RequestParam(required = false) Integer offset,
                                           @RequestParam(required = false) @Range(min = 1, max = 200) Integer limit,
                                           @RequestParam(required = false) @Range(min = 1, max = 4000) Integer radius,
                                           @RequestParam(required = false) String fields,
                                           @RequestParam(required = false) String query){
        // limit, offset, field, sortBy, order, near, radius
        String parameters = "";
        if(fields != null){
            parameters = parameters.concat("&fields=" + fields);
        }
        if(longitude != null && latitude != null){
            parameters = parameters.concat("&near=" + longitude + "," + latitude);
        }
        if(sortBy != null){
            parameters = parameters.concat("&sortBy=" + sortBy);
        }
        if(order != null){
            parameters = parameters.concat("&order=" + order);
        }
        if(near != null){
            parameters = parameters.concat("&near=" + near);
        }
        if(offset != null){
            parameters = parameters.concat("&offset=" + offset);
        }
        if(limit != null){
            parameters = parameters.concat("&limit=" + limit);
        }
        if(radius != null){
            parameters = parameters.concat("&radius=" + radius);
        }
        if(query != null){
            parameters = parameters.concat(ProcessQuery(query));
        }
        System.out.println(parameters);
        String url = baseURL + apiKey + parameters;
        System.out.println(url);

        Payload payload = webClientBuilder.build()
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(Payload.class)
                .block();

        return new ResponseEntity<>(payload, HttpStatus.ACCEPTED);
    }


    @GetMapping("circleSearch")
    @Operation(summary = "Get Zillow data within circle",
            description = "Provide coordinates and radius of circle and return number or properties indicated in limit"
    )
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success"),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(mediaType = "application/json",
            schema = @Schema(type = "object", implementation = ErrorResponse.class))),
            @ApiResponse(responseCode = "500", description = "Zillow Service Exception")
    })
    public ResponseEntity<Properties> getZillowCircleData(@ApiParam( name = "limit", required = true, value = "Number of items to be returned (Constraint: 1 - 200)")
                                                          @RequestParam @Range(min = 1, max = 200) Integer limit,
                                                          @ApiParam( name = "latitude", required = true, value = "Latitude Coordinate of circle (Constraint: -90 <= latitude <= 90")
                                                          @RequestParam @Range(min = -90, max = 90) Float latitude,
                                                          @ApiParam( name = "longitude", required = true, value = "Longitude Coordinate of circle (Constraint: -180 <= longitude <= 180")
                                                          @RequestParam @Range(min = -180, max = 180) Float longitude,
                                                          @ApiParam( name = "radius", required = true, value = "Radius of circle (Constraint: 1 - 4000 miles)")
                                                          @RequestParam @Range(min = 1, max = 4000) Integer radius){
        String url = baseURL + apiKey +
                "&limit=" +limit +
                "&near=" + longitude + "," + latitude +
                "&radius=" + radius;

        Properties properties = webClientBuilder.build()
                .get()
                .uri(url)
                .retrieve()
                .bodyToMono(Properties.class)
                .block();
        return new ResponseEntity<>(properties,HttpStatus.OK);
    }
}