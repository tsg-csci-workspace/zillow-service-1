package com.csci.zillowservice.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ZillowControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getZillowData_isAccepted() throws Exception{
        this.mockMvc.perform(get("/v1/test/zillowTest?limit=1")).andDo(print()).andExpect(status().isAccepted())
                .andExpect(content().string(containsString("\"success\":true")));
    }

    @Test
    void getZillowData_missingParameter() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/zillowTest?limit=")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof MissingServletRequestParameterException);
    }

    @Test
    void getZillowData_invalidParameterType() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/zillowTest?limit=a")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof TypeMismatchException);
    }

    @Test
    void getZillowData_failedConstraint() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/zillowTest?limit=300")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof ConstraintViolationException);
    }

    @Test
    void processQuery() {
        ZillowController controller = new ZillowController();
        String response = ZillowController.ProcessQuery("ListPrice.gt=100,BedroomsTotal.gt=1,YearBuilt.gt=1,RoomsTotal.gt=1");
        assertTrue(response.equals("&ListPrice.gt=100&BedroomsTotal.gt=1&YearBuilt.gt=1&RoomsTotal.gt=1"));
    }

    @Test
    void getData_isAccepted() throws Exception{
        this.mockMvc.perform(get("/v1/test/zillow/search")).andDo(print()).andExpect(status().isAccepted())
                .andExpect(content().string(containsString("\"success\":true")));
    }

    @Test
    void getData_isAcceptedWithParameters() throws Exception{
        this.mockMvc.perform(get("/v1/test/zillow/search?limit=1&latitude=34&longitude=-128&radius=100")).andDo(print()).andExpect(status().isAccepted())
                .andExpect(content().string(containsString("\"success\":true")));
    }

    @Test
    void getData_invalidParameterType() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/zillow/search?limit=a")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof TypeMismatchException);
    }
    @Test
    void getData_failedConstraint() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/zillow/search?limit=300")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof ConstraintViolationException);
    }

    @Test
    void getZillowCircleData_isAccepted() throws Exception{
        this.mockMvc.perform(get("/v1/test/circleSearch?limit=1&latitude=34&longitude=-128&radius=100")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"success\":true")));
    }
    @Test
    void getZillowCircleData_missingParameter() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/circleSearch?limit=")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof MissingServletRequestParameterException);
    }
    @Test
    void getZillowCircleData_invalidParameterType() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/circleSearch?limit=a")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof TypeMismatchException);
    }
    @Test
    void getZillowCircleData_failedConstraint() throws Exception{
        MvcResult result = this.mockMvc.perform(get("/v1/test/circleSearch?limit=300&latitude=34&longitude=-128&radius=100")).andDo(print()).andExpect(status().is4xxClientError())
                .andReturn();
        assertTrue(result.getResolvedException() instanceof ConstraintViolationException);
    }

}